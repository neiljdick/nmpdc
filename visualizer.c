#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <complex.h>
#include <fftw3.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include "visualizer.h"

static const char *fifo = "/tmp/mpd.fifo";
#define FREQ_DISCARD 16

#define SAMPLE_RATE 44100
static unsigned int averaging = 0;
static unsigned int nsamples = SAMPLE_RATE * 2;
static unsigned int dft_input_samples = SAMPLE_RATE;
static unsigned int dft_output_samples = SAMPLE_RATE / 2; /* output is reflected at n/2 */

struct visualizer {
    int fd;
    unsigned int nsamples;
    int16_t *audio_buf;
    double *in;
    double *res;
    complex *out;
    fftw_plan plan;
    pthread_mutex_t lock;
    bool running;
    pthread_t thread;
};



static struct visualizer vis;

int
visualizer_init(void) {
    /* open audio pipe */
    vis.fd = open(fifo, O_RDONLY | O_NONBLOCK);
    if (vis.fd < 0) {
        perror("Pipe Open\n");
        return -1;
    }
    fftw_init_threads();

    /* allocate buffers for audio processing */
    /* Use fftw allocators for simd allignment */
    /* buf - raw 16bit audio from fifo */
    /* in - double - average of 2 audio channels, input to dft */
    /* out - output of dft, complex */
    vis.audio_buf = malloc(nsamples * sizeof(int16_t));
    vis.in = fftw_alloc_real(dft_input_samples);
    vis.out = fftw_alloc_complex(dft_input_samples);
    vis.res = calloc(dft_output_samples, sizeof(double));
    if (vis.audio_buf == NULL ||
        vis.in == NULL ||
        vis.out == NULL) {
        printf("Error, allocating memory\n");
        return -1;
    }
    fftw_plan_with_nthreads(4);

    vis.plan = fftw_plan_dft_r2c_1d(dft_input_samples,
                                     vis.in,
                                     vis.out,
                                     FFTW_ESTIMATE);

    pthread_mutex_init(&vis.lock, NULL);
    averaging = 0;
    return 0;
}



static void
visualizer_trigger(void) {
    static double max = 0;
    /* read audio samples */

    int n = read(vis.fd, vis.audio_buf, nsamples * sizeof(int16_t));

    pthread_mutex_lock(&vis.lock);
    /* average the audio samples and populate 'in' buffer */
    for (size_t i = 0; i < dft_input_samples; ++i) {
        vis.in[i] = 0.0;
        if (i < n/2) {
            assert(i*2 < nsamples);
            assert(i*2+1 < nsamples);
            vis.in[i] = vis.audio_buf[i*2];
            vis.in[i] += vis.audio_buf[i*2 + 1];
            vis.in[i] /= 2.0;
        }
    }
    /* trigger dft */
    fftw_execute(vis.plan);

    for (size_t i = 0; i < dft_output_samples; ++i) {
        vis.in[i] = cabs(vis.out[i]);
        if (vis.in[i] > max) {
            max = vis.in[i];
        }
    }
    /* normalize the results between 0 and 1 */
    for (size_t i = 0; i < dft_output_samples; ++i) {
        if (max > 0) {
            vis.in[i] /= max;
            vis.res[i] += vis.in[i];
        }
    }
    averaging++;
    pthread_mutex_unlock(&vis.lock);
    max /= 1.1;                  /* gradually reduce the max */
    return;
}


void
visualizer_free(void) {
    if (vis.in){
        fftw_free(vis.in);
    }
    if (vis.out) {
        fftw_free(vis.out);
    }
    if (vis.audio_buf) {
        free(vis.audio_buf);
    }
    fftw_destroy_plan(vis.plan);
    fftw_cleanup_threads();
    close(vis.fd);
}

static inline bool
check_running(void) {
    bool ret = false;
    pthread_mutex_lock(&vis.lock);
    ret = vis.running;
    pthread_mutex_unlock(&vis.lock);    
    return ret;
}

static void *
visualizer_thread(void* arg) {
    /* poor mans initial sync ? */

    while(check_running()) {
        visualizer_trigger();        
        usleep(20000);
    }
    pthread_exit(NULL);
}

int
visualizer_start(void) {

    vis.running = true;
    pthread_create(&vis.thread,
                   NULL,
                   visualizer_thread,
                   NULL);

    return 0;
}

int
visualizer_stop(void) {
    if (vis.running){
        pthread_mutex_lock(&vis.lock);
        vis.running = false;
        pthread_mutex_unlock(&vis.lock);
        pthread_join(vis.thread, NULL);
    }
    return 0;
}

int visualizer_get_graph(int *bar,
                         unsigned int width,
                         unsigned int height) {
    assert(bar);
    assert(width > 0);
    assert(height > 0);
    /* work out the number of bins */
    unsigned int samples_in_bin = (dft_output_samples/FREQ_DISCARD) / width;

    /* average samples into a bin */
    pthread_mutex_lock(&vis.lock);
    for (size_t i = 0; i < width; ++i) {
        double bin = 0.0;

        for (size_t j = i*samples_in_bin; j < (i+1)*samples_in_bin; ++j) {
            assert(j < dft_output_samples);
            averaging = (averaging == 0 ? 1 : averaging);
            bin += vis.res[j]/averaging;
            vis.res[j] = 0;
        }
        bin /= samples_in_bin;
        bar[i] = bin * height;
        if (bar[i] > height) {
            bar[i] = height;
        }
    }
    averaging = 0;
    pthread_mutex_unlock(&vis.lock);
    
    return 0;
}
