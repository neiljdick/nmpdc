#ifndef __UI_H__
#define __UI_H__

#include <mpd/client.h>

int
ui_init(void);

int
ui_loop(struct mpd_connection *mpd);


#endif
