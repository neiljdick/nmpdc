#ifndef __MPD_H__
#define __MPD_H__

struct mpd_display_info {
    char *song_title;
    char *band_name;
    char *directory;
    char *player_status;
};

struct mpd_display_info
update_display_info (struct mpd_connection *mpd);

void
mpd_play(struct mpd_connection *mpd);

void
mpd_stop(struct mpd_connection *mpd);

void
mpd_next(struct mpd_connection *mpd);

void
mpd_prev(struct mpd_connection *mpd);

#endif
