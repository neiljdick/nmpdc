/* An mpd client written in C and lua! */

#include <stdio.h>
#include <mpd/client.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include "ui.h"
#include "mpd.h"

const char *host = "localhost";
const uint32_t port = 6600;

int 
main(int argc, char **argv) {
    printf("Welcome to nmpdc!\n");
    printf("Opening connection to mpd.. \n");
    struct mpd_connection *mpd_conn =
        mpd_connection_new(host,
                           port,
                           0);
    if (mpd_conn == NULL) {
        printf("Error in connection \n");
    } else {
        /* check for error */
        if (mpd_connection_get_error(mpd_conn) != MPD_ERROR_SUCCESS) {
            printf("MPD Error: %s\n", mpd_connection_get_error_message(mpd_conn));
            return -1;
        }
        else {
            printf("MPD Connection successful! \n");
            int i;
            for(i=0;i<3;i++) {
                printf("version[%i]: %i\n",i,
                       mpd_connection_get_server_version(mpd_conn)[i]);
            }
        }
    }

    ui_init();
    ui_loop(mpd_conn);

    if (mpd_conn) {
        mpd_connection_free(mpd_conn);
    }
    return 0;
}
