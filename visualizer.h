#ifndef __VISUALIZER_H__
#define __VISUALIZER_H__
#include <complex.h>
struct visualizer_freq {
    double *freq;              /* freq output of dft */
    unsigned int n;            /* number of freq samples */
};

int
visualizer_init(void);
int
visualizer_start(void);
int
visualizer_stop(void);

/* bar needs to be allocated to be width numbers long */
int visualizer_get_graph (int *bar,
                          unsigned int width,
                          unsigned int height);
void
visualizer_free(void);


#endif
