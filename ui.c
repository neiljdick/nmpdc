#include <stdio.h>
#include <stdbool.h>
#include <ncurses.h>
#include <stdlib.h>
#include <unistd.h>
#include "ui.h"
#include <assert.h>
#include "mpd.h"
#include "visualizer.h"


static unsigned int getc_timeout = 200; 
static bool visualizer_enable;

#define GRAPH_BINS width


int
ui_init(void) {
    initscr();
    noecho();
    curs_set(FALSE);
    cbreak();
    timeout(getc_timeout);

    /* setup the visualizer,
       dont bail out entirely if it fails,
       just disable it
     */
    if (visualizer_init()) {
        visualizer_enable = false;
    } else {
        visualizer_enable = true;
    }

    return 0;
}
static inline void
free_display_info(struct mpd_display_info info) {
    free(info.song_title);
    free(info.band_name);
    free(info.directory);
    /* status doesnt need to be freed */
    return;
}

static void
bring_up_menu(struct mpd_connection *mpd) {
    
}

static void
ui_cleanup(void) {
    endwin();
    visualizer_stop();
    visualizer_free();
}


static void
show_graph(int *bars, int width, int height) {
    erase();        
    for (size_t i = 0; i < width; i++) {
        assert(bars[i] <= height);
        for (size_t j = bars[i]; j > 0; j--) {
            mvaddch(j,i,'#');
        }
    }
    move(0, 0);
    refresh();
}



static int
ui_update(struct mpd_connection *conn){
    assert(conn);
    struct mpd_display_info info =
        update_display_info(conn);

    /* mvprintw(0,0, "Got title: %s\n artist: %s\n status: %s", */
    /*        info.song_title, */
    /*        info.band_name, */
    /*        info.player_status); */

    int height, width;
    getmaxyx(stdscr, height, width);
    int bar[width];
    visualizer_get_graph(bar, width, height);
    show_graph(bar, width, height);

    free_display_info(info);
    return 0;
}

int
ui_loop(struct mpd_connection *mpd) {
    assert(mpd);
    bool done = false;
    visualizer_start();

    while (! done) {
        int c = getch();

        if (c == ERR) {
            /* timeout in getch */
            ui_update(mpd);
            continue;
        }
        switch (c) {
        case 'd':
            bring_up_menu(mpd);
            break;
        case 'q':
            done = true;
            break;
        case 'p':
            mpd_play(mpd);
            break;
        case 's':
            mpd_stop(mpd);
            break;
        case 'n':
            mpd_next(mpd);
            break;
        case 'b':
            mpd_prev(mpd);
            break;
        default:
            break;
        }
    }

    ui_cleanup();
    return 0;
}
