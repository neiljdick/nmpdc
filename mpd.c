#include <mpd/client.h>
#include "mpd.h"
#include <ncurses.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>



static inline const char *
get_tag(const struct mpd_song *song,enum mpd_tag_type type) {
    return mpd_song_get_tag(song,type, 0);
}

static char*
get_player_status(struct mpd_connection *mpd) {
    assert(mpd);
    char *status_str = "Unknown";
    struct mpd_status *status = NULL;

    mpd_send_status(mpd);
    status = mpd_recv_status(mpd);
    if (status) {
        switch (mpd_status_get_state(status)) {
        case MPD_STATE_PLAY:
            status_str = "Playing";
            break;
        case MPD_STATE_PAUSE:
            status_str = "Paused";
            break;
        case MPD_STATE_STOP:
            status_str = "Stopped";
        default:
            status_str = "Unknown";
            break;
        }
        mpd_status_free(status);
    }
    return status_str;
}

static char*
get_song_tag(struct mpd_connection *mpd,
             enum mpd_tag_type tag) {
    assert(mpd);
    char *tag_string = NULL;
    struct mpd_song *song = NULL;    
    mpd_send_current_song(mpd);
    while ((song = mpd_recv_song(mpd)) != NULL) {
        tag_string = strdup(get_tag(song, tag));
        mpd_song_free(song);
    }
    return tag_string;
}


struct mpd_display_info
update_display_info (struct mpd_connection *mpd) {
    assert(mpd);

    struct mpd_display_info info = { 
        .song_title = get_song_tag(mpd, MPD_TAG_TITLE),
        .band_name = get_song_tag(mpd, MPD_TAG_ARTIST),
        .player_status = get_player_status(mpd),
        .directory = NULL,
    };
    
    return info;
}

void
mpd_play(struct mpd_connection *mpd) {
    mpd_run_play(mpd);
    return;
}

void
mpd_stop(struct mpd_connection *mpd) {
    mpd_run_stop(mpd);
    return;
}

void
mpd_next(struct mpd_connection *mpd){
    mpd_run_next(mpd);
}

void
mpd_prev(struct mpd_connection *mpd){
    mpd_run_previous(mpd);
}
