TARGET = nmpdc
LIBS = -lmpdclient -lncurses -lfftw3_threads -pthread -lfftw3 -lm
CC = gcc
# Debug
CFLAGS = -g -Wall --std=c11 -D_DEFAULT_SOURCE -pthread
# Release 
# CFLAGS = -02 -Wall --std=c11
.PHONY: default all clean

default: $(TARGET)
all: default

OBJECTS = $(patsubst %.c, %.o, $(wildcard *.c))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

.PRECIOUS: $(TARGET) $(OBJECTS)

$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -Wall $(LIBS) -o $@

clean:
	-rm -f *.o
	-rm -f $(TARGET)
